/**
 * Question #1
 * Created for a coding test.
 * @author Nate DiPiazza
 * Contact: natedipiazza@gmail.com
 *
 */
public class StringtoLong {

  /**
   * Main class tests the correctness of method stringToLong
   * @param args
   * @throws Exception 
   */
  public static void main(String[] args) throws Exception {
    //1) test handling of zero chars
    if( stringToLong("1009") != 1009){
      System.err.println("Failed Test 1)");
      System.exit(1);
    }
    //2) negative numbers
    if( stringToLong("-165") != -165){
      System.err.println("Failed Test 2)");
      System.exit(1);
    }
    //3) String can be of max long value size
    if( stringToLong(Long.toString(Long.MAX_VALUE)) != Long.MAX_VALUE){
      System.err.println("Failed Test 3)");
      System.exit(1);
    }
    //4) method can handle commas or periods
    if( stringToLong("1,000,000") != 1000000){
      System.err.println("Failed Test 4)");
      System.exit(1);
    }
    //5) invalid input should raise an exception
    try {
      stringToLong("1-333");
    } catch (Exception e) {
      //do nothing
    }
    try {
      stringToLong("1.333");
    } catch (Exception e) {
      System.out.println("All tests passed!");
      System.exit(0);
    }
    //exception should be raised
    System.err.println("Failed Test 5)");
  }
  
  /**
   * Converts a string to a long without using a build in method call.
   * Allowable Formats: "123" || "1,234" || 0002 || -111
   * @param s
   * @return converted String of type long
   * @throws Exception 0-9  , and - allowed
   */
  static long stringToLong(String s) throws Exception{
    /* code goes here to convert a string to a long */
    long base = 1;
    long number = 0;
    int ascii_zero = 48;
    int ascii_nine = 57;
    char[] s_array = s.toCharArray();
    
    for (int i = s_array.length-1; i >= 0; i--) {
      if( (s_array[i] > ascii_zero) && (s_array[i] <= ascii_nine) ){
        int num = (int) s_array[i] - ascii_zero;
        number += ((long)num)*base;
      // only non-numeric characters , and - are allowed
      } else if((s_array[i] != ',') && (s_array[i] != ascii_zero) && (s_array[i] != '-')){
        throw new Exception();
      }
      //validate that - only appears first
      if((s_array[i] == '-') && (i > 0)){
        throw new Exception();
      }
      // comma is just placeholder
      if(s_array[i] != ','){
        base *= 10;
      }
      
    }
    if(s_array[0] == '-'){
      number *= -1;
    }
    return number;
  }

}
