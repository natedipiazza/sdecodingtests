Question #1 String to Long conversion problem

The program I wrote to solve this problem is in Java.
To solve this problem I took as input the numeric string and scanned through the digits from least significant to most significant.
I used an incrementing base-10 value (1,10,100...) which I multiplied by the current digit. I then added this to a running total as I worked through the string.
Other issues handled include: validating numericality, allowing negative numbers, allowing some common numeric string inputs like: '1,000', and stripping trailing zeroes.
The main Java class contains several tests that I wrote to verify correctness. One limit the code has is that it cannot convert string-floats to longs. If I were to write more validations I would like to tighten up the comma-validation e.g. '1,000,000' -- would be allowed and '55,6,7777,' --would raise an exception.

To run the tests: java StringtoLong

Question #2 Trinary Tree

This program is also in Java. There are three files: Node.java, Tree.java, and TrinaryTree.java. The Node class stores the values and has pointers to all possible children and a back-pointer to the parent.  The Tree class contains a root node pointer and the implementation of insert and delete operations.  TrinaryTree is the main class and contains a test suite to verify correctness of the program.

To run the tests: java TrinaryTree
 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

I hope you like the code. Let me know if you have any questions.

-Nate DiPiazza