/**
 * Created for a coding test.
 * @author Nate DiPiazza
 * Contact: natedipiazza@gmail.com
 *
 */
public class Node {

  private int value;
  private Node leftChild = null;
  private Node middleChild = null;
  private Node rightChild = null;
  private Node parent = null;

  public Node(int value) {
    super();
    this.value = value;
  }
  
  public void clear(){
    leftChild = null;
    rightChild = null;
    middleChild = null;
  }
  
  public Node getParent() {
    return parent;
  }

  public void setParent(Node parent) {
    this.parent = parent;
  }
  
  public int getValue() {
    return value;
  }

  public void setValue(int value) {
    this.value = value;
  }

  public Node getLeftChild() {
    return leftChild;
  }

  public void setLeftChild(Node leftChild) {
    this.leftChild = leftChild;
  }

  public Node getMiddleChild() {
    return middleChild;
  }

  public void setMiddleChild(Node middleChild) {
    this.middleChild = middleChild;
  }

  public Node getRightChild() {
    return rightChild;
  }

  public void setRightChild(Node rightChild) {
    this.rightChild = rightChild;
  }
  
}
