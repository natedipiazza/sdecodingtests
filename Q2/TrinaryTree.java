/**
 * Question #2
 * Created for a coding test.
 * @author Nate DiPiazza
 * Contact: natedipiazza@gmail.com
 *
 */
public class TrinaryTree {

  /**
   * This main class includes tests for a
   * Tri-nary tree implementation.
   * @param args
   */
  public static void main(String[] args) {
    //test insertion///////////////////////////////////////////////////////////
    int[] values = {5, 4, 9, 5, 7, 2, 2, 5};
    Tree tree = loadTree(values);
    Node root = tree.getRoot();
    if( (root.getValue() != 5) && (root.getParent() != null) ){
      System.err.println("Insertion test failed: bad root");
      System.exit(1);
    }
    //check on the kids
    if( (root.getLeftChild().getValue() != 4) && (root.getMiddleChild().getValue() != 5) && (root.getRightChild().getValue() != 9)){
      System.err.println("Insertion test failed: invalid root children");
      System.exit(1);
    }
    //check deepest node
    Node deepNode = root.getLeftChild().getLeftChild().getMiddleChild();
    if( (deepNode.getValue() != 2) && (deepNode.getParent().getValue() != 2) ){
      System.err.println("Insertion test failed: deep node was not inserted properly");
      System.exit(1);
    }
    //test deletion////////////////////////////////////////////////////////////
    // delete a leaf node
    tree.delete(7);
    if(root.getRightChild().getLeftChild() != null){
      System.err.println("Deletion test failed: leaf not deleted");
      System.exit(1);
    }
    // leaf is middle node
    tree.delete(2);
    if(root.getLeftChild().getLeftChild().getMiddleChild() != null){
      System.err.println("Deletion test failed: middle leaf not deleted");
      System.exit(1);
    }
    // Left child test
    // reset tree
    tree = loadTree(values); //same data set
    root = tree.getRoot();
    tree.delete(9);
    // 9 should be replaced with child:7
    if( (root.getRightChild().getValue() != 7) && (root.getRightChild().getParent().getValue() != 5)){
      System.err.println("Deletion test failed: node with left child");
      System.exit(1);
    }
    // Right child test
    int[] rValues = {5, 4, 9, 5, 12, 2, 2, 5}; //changed values[] 7 is now 12
    tree = loadTree(rValues); //same data set
    root = tree.getRoot();
    tree.delete(9);
    // 9 should be replaced with child:12
    if( (root.getRightChild().getValue() != 12) && (root.getRightChild().getParent().getValue() != 5)){
      System.err.println("Deletion test failed: node with right child");
      System.exit(1);
    }
    // Both children test
    int[] bValues = {5, 4, 9, 5, 7, 12, 2, 2, 5}; // 9 has children: 7 and 12
    tree = loadTree(bValues); //same data set
    root = tree.getRoot();
    tree.delete(9);
    if( (root.getRightChild().getValue() != 12) && (root.getRightChild().getParent().getValue() != 5)){
      System.err.println("Deletion test failed: node with both children (sucessor)");
      System.exit(1);
    }
    // now a trickier one
    int[] modValues = {10,8,15,9,2,3,1}; // novel data set
    tree = loadTree(modValues); //same data set
    root = tree.getRoot();
    tree.delete(8);
    if( (root.getLeftChild().getValue() != 3) && (root.getLeftChild().getParent().getValue() != 10)){
      System.err.println("Deletion test failed: node with both children (predesessor)");
      System.exit(1);
    }
    // validate root deletion
    tree.delete(10);
    if(tree.getRoot().getValue() != 15){
      System.err.println(root.getValue());
      System.exit(1);
    }
    System.out.println("All tests passed!");
  }
  /**
   * Given an array of values for the nodes,
   * create a tri-nary tree and load it with some test data
   * @param values
   * @return
   */
  public static Tree loadTree(int[] values){
    Tree tree = new Tree();
    for (int i = 0; i < values.length; i++) {
      tree.insert(values[i]);
    }
    return tree;
  }

}
