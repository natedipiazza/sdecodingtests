
/**
 * Created for a coding test.
 * @author Nate DiPiazza
 * Contact: natedipiazza@gmail.com
 *
 */
public class Tree {
  
  private Node root = null;
  
  /**
   * Given an integer value, add insert it into a tri-nary tree.
   * A tri-nary tree is much like a binary
   * tree but with three child nodes for each parent instead of two
   * with the left node being values
   * less than the parent, the right node values greater than the parent,
   * and the middle nodes values equal to the parent.
   * @param value
   */
  public void insert(int value){
    Node node = new Node(value);
    //if no nodes initialize
    if(root == null){
      root = node;
    } else {
      Node nextNode = getNextOrInsert(root, node);
      while(nextNode != null){
        // call recursively until node is inserted
        nextNode = getNextOrInsert(nextNode, node);
      }
    }
  }
  /**Delete the node from the tree with the given value.
   * If the value does not exist, inform user.
   * If found, find the deepest middle note with that
   * value and delete it from tree. <br/>
   * <i>Convention: when deleting a root node replace with its in-order successor.</i>
   * @param value
   */
  public void delete(int value){
    Node curNode = root;
    //traverse the tree. if found set node to null, else print messasge and return
    while(true){
      if(value < curNode.getValue()){
        if(curNode.getLeftChild() == null){
          notFound(value);
          break;
        } else {
          curNode = curNode.getLeftChild();
        }
      }else if (value == curNode.getValue()) {
        if(curNode.getMiddleChild() == null){
          //We have found the node
          realignTree(curNode);
          break;
        } else {
          curNode = curNode.getMiddleChild();
        }
      }else {
        if(curNode.getRightChild() == null){
          //curNode = null;
          notFound(value);
          break;
        } else {
          curNode = curNode.getRightChild();
        }
      }
    }
  }
  //for testing correctness of tree operations
  public Node getRoot() {
    return root;
  }

  /**
   * This method either inserts the node in the proper
   * place or return the next node to be searched
   * null is flag for base case else return next node**/
  private Node getNextOrInsert(Node curNode, Node newNode){
    Node nextNode = null;
    //traverse tree to find insertion point
    if(newNode.getValue() < curNode.getValue()){
      //check left child
      if(curNode.getLeftChild() == null){
        curNode.setLeftChild(newNode);
        newNode.setParent(curNode);
      } else {
        nextNode = curNode.getLeftChild();
      }
    } else if (newNode.getValue() == curNode.getValue()) {
      //check middle child
      if(curNode.getMiddleChild() == null){
        curNode.setMiddleChild(newNode);
        newNode.setParent(curNode);
      } else {
        nextNode = curNode.getMiddleChild();
      }
    } else {
      // must be right child
      if(curNode.getRightChild() == null){
        curNode.setRightChild(newNode);
        newNode.setParent(curNode);
      } else {
        nextNode = curNode.getRightChild();
      }
    }
    return nextNode;
  }
  
  private void notFound(int value){
    System.out.println(value + ": does not exist.");
  }
  
  /**
   * When a deleted node that has children
   * there is some bookkeeping required.
   * This method realigns the tree so
   * that the tri-nary tree properties hold
   * @param node
   */
  private void realignTree(Node node){
    Node parent =  node.getParent();
    boolean left = false;
    //for root, directionality doesn't matter
    if(node.getParent() != null){
      if(node.getValue() < parent.getValue()){
        left = true;
      }
    }
    //3 cases: leaf, 1 child [left,right], and both children/////
    if( isLeaf(node) ){
      //find which way the node came from
      if(node.getValue() == parent.getValue()){
        parent.setMiddleChild(null);
      }else if(left){
        parent.setLeftChild(null);
      } else {
        parent.setRightChild(null);
      }
    } else if ( isLeft(node) ) {
      if(left){
        parent.setLeftChild(node.getLeftChild());
        parent.getLeftChild().setParent(parent);
      } else {
        parent.setRightChild(node.getLeftChild());
        parent.getRightChild().setParent(parent);
      }
    } else if ( isRight(node) ) {
      if(left){
        parent.setLeftChild(node.getRightChild());
        parent.getLeftChild().setParent(parent);
      } else {
        parent.setRightChild(node.getRightChild());
        parent.getRightChild().setParent(parent);
      }
    }else if ( hasBoth(node) ) {
      if(left){
        //Find the in-order predecessor
        Node curNode = node.getLeftChild();
        while(curNode.getRightChild() != null){
          curNode = curNode.getRightChild();
        }
        // link deleted nodes right child to predecessor
        curNode.setRightChild(node.getRightChild());
        curNode.getRightChild().setParent(curNode);
        // is there a predecessor?
        if(!curNode.getParent().equals(node)){
          if (curNode.getLeftChild() != null) {
            // link pred-parent's right child as pred's left child (there is never a right)
            curNode.getParent().setRightChild(curNode.getLeftChild());
            curNode.getParent().getRightChild().setParent(curNode.getParent());
          //avoid null pointer error//
          } else {
            curNode.getParent().setRightChild(null);
          }
          // now link deleted nodes left child to predecessor
          curNode.setLeftChild(node.getLeftChild());
          curNode.getLeftChild().setParent(curNode);
        }
        // link deleted nodes parent to predecessor
        curNode.setParent(node.getParent());
        // check if deleted node is root
        if(curNode.getParent() == null){
          root = curNode;
        } else {
          curNode.getParent().setLeftChild(curNode);
        }  
      } else {
        //Find the in-order successor
        Node curNode = node.getRightChild();
        while(curNode.getLeftChild() != null){
          curNode = curNode.getLeftChild();
        }
        // set deleted nodes left child as successor's left child
        curNode.setLeftChild(node.getLeftChild());
        curNode.getLeftChild().setParent(curNode);
        // is there a successor?
        if(!curNode.getParent().equals(node)){
          if (curNode.getRightChild() != null) {
            // link successor's right child to succ-parent's left child
            curNode.getParent().setLeftChild(curNode.getRightChild());
            curNode.getParent().getLeftChild().setParent(curNode.getParent());
          } else {
            curNode.getParent().setLeftChild(null);
          }
          // now set deleted nodes right child as successor's right child
          curNode.setRightChild(node.getRightChild());
          curNode.getRightChild().setParent(curNode);
        }
        // link deleted nodes parent to predecessor
        curNode.setParent(node.getParent());
        if(curNode.getParent() == null){
          root = curNode;
        } else {
          curNode.getParent().setRightChild(curNode);
        }
      }
    }else {
      //error
      System.err.println("Error: method-realign tree. unexpected case");
      System.exit(1);
    }
    node = null; //finalize node deletion
      
     
  }
  
  private boolean isLeaf(Node node){
    boolean leaf = false;
    if( (node.getLeftChild() == null) && (node.getRightChild() == null) ){
      leaf = true;
    }
    return leaf;
  }
  
  private boolean isLeft(Node node){
    boolean left = false;
    if( (node.getLeftChild() != null) && (node.getRightChild() == null) ){
      left = true;
    }
    return left;
  }
  
  private boolean isRight(Node node){
    boolean right = false;
    if( (node.getLeftChild() == null) && (node.getRightChild() != null) ){
      right = true;
    }
    return right;
  }
  
  private boolean hasBoth(Node node){
    boolean both = false;
    if( (node.getLeftChild() != null) && (node.getRightChild() != null) ){
      both = true;
    }
    return both;
  }

}
